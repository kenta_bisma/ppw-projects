from django.shortcuts import render, redirect
from django.views.generic.base import RedirectView

from datetime import datetime
class SearchRedirectView(RedirectView):
  url = 'https://google.com/search?q=%(term)s'

def index(request):
    return render(request, 'story_4/index.html', {'caption':'Story 4'})

def dummy(request):
    return render(request, 'story_4/dummy.html', {'caption':'Story 4 Dummy'})

def redirect(request):
    return render(request, 'story_4/redirect.html', {'caption':'Story 4 Redirect'})

def demo(request):
    now = str(datetime.date(datetime.now()))
    return render(request, 'story_4/demo.html', {'caption':'Story 4 Demo', 'date':now})