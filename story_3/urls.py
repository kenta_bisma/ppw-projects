from django.urls import path, include

from . import views

app_name = 'story_3'

urlpatterns = [
    path('', views.index, name='story_3'),
    path('drafts/', include('story_3.drafts_urls')),
    path('wireframe/', views.wireframe, name='story_3_wireframe'),
    path('main/', views.main, name='story_3_main'),
]