from django.db import models

class Activity(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)

class Person(models.Model):
    id = models.AutoField(primary_key=True)
    person_name = models.CharField(max_length=30)
    activity = models.ForeignKey(Activity, related_name="people", on_delete=models.CASCADE)