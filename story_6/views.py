from story_6.models import Activity, Person
from django.http import request
from django.shortcuts import redirect, render
from .forms import ActivityForm, PersonForm

context = {}
# Create your views here.
def hub(request):
    context['caption'] = "Story 6"

    if request.method == "POST":
        form = ActivityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/story_6/")

        person_form = PersonForm(request.POST)
        if person_form.is_valid():
            person_form.save()
            return redirect("/story_6/")
    else:
        form = ActivityForm()
        person_form = PersonForm()
    context["activity_form"] = form
    context["person_form"] = person_form

    activities = Activity.objects.all()
    context["activities"] = activities
    return render(request, "story_6/hub.html", context)

def delete_activity(request, pk):
    activity = Activity.objects.get(id=pk)
    if request.method == 'POST':
        activity.delete()
        return redirect('/story_6/')

def delete_person(request, pk):
    person = Person.objects.get(id=pk)
    if request.method == 'POST':
        person.delete()
        return redirect('/story_6/')