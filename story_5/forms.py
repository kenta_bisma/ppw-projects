from django import forms
from story_5.models import MatKul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatKul
        fields = "__all__"
    
