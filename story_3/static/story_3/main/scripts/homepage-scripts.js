function scrollFunctionHomepage() {
    var homepageText = document.getElementById("homepage-text");
    if (document.body.scrollTop > homepageText.offsetHeight/2 || document.documentElement.scrollTop > homepageText.offsetHeight/2) {
        homepageText.style.opacity = "0";
    } else {
        homepageText.style.opacity = "1";
    }
}

function homepageResponse(media) {

    var h1 = document.getElementsByTagName("h1")[0];
    var h2 = document.getElementsByTagName("h2")[0];
    if (media.matches) {
        h1.style.fontSize = "2.5rem";
        h1.style.lineHeight = "3rem";
        h2.style.fontSize = "0px";
        h2.style.lineHeight = "0px";
    } else {
        h1.style.fontSize = "3rem";
        h1.style.lineHeight = "3.5rem";
        h2.style.fontSize = "1.5rem";
        h2.style.lineHeight = "2rem";
    }
}

var media = window.matchMedia("(max-width: 800px)");
homepageResponse(media)
media.addListener(homepageResponse)