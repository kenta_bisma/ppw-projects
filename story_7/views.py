from django.shortcuts import render

# Create your views here.
def index(request):
    context ={'caption':"Story 7"}
    return render(request, "story_7/index.html", context)