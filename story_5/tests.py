from story_5.models import MatKul
from django.urls.base import resolve
from django.test import TestCase, Client
from . import views, models, forms

# class Story5Test(TestCase):
#     def test_story_5_url_is_exist(self):
#         response = Client().get('/story_5/')
#         self.assertEqual(response.status_code, 200)

#     def test_story_5_using_index_template(self):
#         response = Client().get('/story_5/')
#         self.assertTemplateUsed(response, 'story_5/index.html')
    
#     def test_story_5_using_index_func(self):
#         found = resolve('/story_5/')
#         self.assertEqual(found.func, views.index)

# # Model testing
# class Story5ModelTest(TestCase):
#     def test_story_5_can_create_new_model(self):
#         new_matkul = MatKul.objects.create(
#             name = "Test",
#             lecturer = "Test",
#             classroom = "Test",
#             semester = 1,
#             year = 2019,
#             sks = 4,
#             difficulty = "?",
#             desc = "Test description"
#         )

#         counting_all_available_matkul = MatKul.objects.all().count()
#         self.assertEqual(counting_all_available_matkul, 1)