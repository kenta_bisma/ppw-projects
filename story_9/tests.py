from django.http import response
from django.test import TestCase
from django.test.client import Client
from django.urls import path
from django.urls.base import resolve
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class TestStory9(TestCase):
    
    def test_set_up(self):
        self.client = Client()
        self.url = path('', views.index, name='story_9')
    
    def test_using_template(self):
        response = Client().get("/story_9/")
        self.assertTemplateUsed(response, "story_9/index.html")
    
    def test_login_view(self):
        found = resolve("/story_9/login/")
        self.assertEqual(found.func, views.login)

    def test_login_template(self):
        response = self.client.get("/story_9/login/")
        self.assertTemplateUsed(response, "story_9/login.html")
    
    def test_signup_view(self):
        found = resolve("/story_9/signup/")
        self.assertEqual(found.func, views.signup)
    
    def test_signup_template(self):
        response = self.client.get("/story_9/signup/")
        self.assertTemplateUsed(response, "story_9/signup.html")
    
    def test_signup_POST(self):
        users = User.objects.all().count()
        self.assertEqual(users, 0)
        response = self.client.post("/story_9/signup/", data= {
            'username':'KadalTerbang123',
            'password1':'AAAAAAAAAAAAAAAAAAAAAAA123098',
            'password2':'AAAAAAAAAAAAAAAAAAAAAAA123098'
        })

        self.assertEqual(response.status_code, 302)

        users = User.objects.all().count()
        self.assertEqual(users, 1)
    
    def test_logout_view(self):
        found = resolve("/story_9/logout/")
        self.assertEqual(found.func, views.logout)
    

    '''
    User logic is used here
    '''
    def dummyUser(self, login=False):
        username = 'KadalTerbang123'
        password = 'AAAAAAAAAAAAAAAAAAAAAAA123098'
        self.client.post("/story_9/signup/", data= {
            'username': username,
            'password1':password,
            'password2':password
        })
        if login:
            self.client.login(username=username, password=password)

    def test_index_logged(self):
        self.dummyUser(login=True)
        response = self.client.get("/story_9/")
        self.assertContains(response, "KadalTerbang123")
    
    def test_login_POST_success(self):
        self.dummyUser()
        username = 'KadalTerbang123'
        password = 'AAAAAAAAAAAAAAAAAAAAAAA123098'
        response = self.client.post(
            "/story_9/login/",
            data= {
            'username': username,
            'password':password,
            }
        )
        self.assertEqual(response.status_code, 302)

    
    def test_login_POST_fail(self):
        self.dummyUser()
        username = 'KadalTerbang123'
        password = 'AAAAAAAAAAAAAAAAAAAAAAA123097'
        response = self.client.post(
            "/story_9/login/",
            data= {
            'username': username,
            'password':password,
            }
        )

        self.assertContains(response, "Wrong")
    
    def test_logout(self):
        self.dummyUser(login=True)
        response = self.client.get("/story_9/logout/")
        self.assertEqual(response.status_code, 302)

