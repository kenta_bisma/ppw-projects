from django.test import TestCase
from django.conf.urls import url
from django.test.client import Client
from django.urls import path
from . import views

# Create your tests here.
class TestStory8(TestCase):

    def test_set_up(self):
        self.client = Client()
        self.url = path('', views.index, name='story_8')
    
    def test_using_template(self):
        response = Client().get("/story_8/")
        self.assertTemplateUsed(response, "story_8/index.html")
    
    def test_search_GET_not_ajax(self):
        response = Client().get("/story_8/search/?q=")
        self.assertEqual(response.status_code, 302)