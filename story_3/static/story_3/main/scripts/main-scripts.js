window.onscroll = function() {scrollFunction(), scrollFunctionHomepage()};

function captResponse(media) {

    var capts = document.getElementsByClassName("caption");
    if (media.matches) {
        for (var i = 0; i < capts.length; i++) {
            capts[i].style.fontSize = "1rem";
            capts[i].style.padding = "0.25rem 1rem";
        }
    } else {
        for (var i = 0; i < capts.length; i++) {
            capts[i].style.fontSize = "1.25rem";
            capts[i].style.padding = "0.5rem 2rem";
        }
    }
}

var media = window.matchMedia("(max-width: 800px)");
captResponse(media)
media.addListener(captResponse)