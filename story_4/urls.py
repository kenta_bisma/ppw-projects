from django.urls import path

from . import views

app_name = 'story_4'

urlpatterns = [
    path('', views.index, name='story_4'),
    path('dummy/', views.dummy, name='story_4_views_dummy'),
    path('redirect/', views.redirect, name='story_4-redirect'),
    path('redirect/<term>/', views.SearchRedirectView.as_view()),
    path('demo/', views.demo, name='story_4-demo'),
]