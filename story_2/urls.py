from django.urls import path

from . import views

app_name = 'story_2'

urlpatterns = [
    path('', views.index, name='story_2'),
    path('flow/', views.flow, name='story_2_flow'),
    path('wireframe/', views.wireframe, name='story_2_wireframe'),
    path('prototype/', views.prototype, name='story_2_prototype'),
]