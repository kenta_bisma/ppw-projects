from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.
class MatKul(models.Model):
    SEMESTERS = [
        ('1', 'Odd'),
        ('2', 'Even'),
    ]

    YEARS = [
        (2019, '2019/2020'),
        (2020, '2020/2021'),
        (2021, '2021/2022'),
        (2022, '2022/2023'),
    ]
    UNKNOWN = '?'
    DIFFICULTIES = [
        (UNKNOWN, '❓ ???'),
        ('1', '✔️ EZ'),
        ('2', '😐 Fair'),
        ('3', '💥 Tough'),
    ]
    id = models.AutoField(primary_key=True)

    name = models.CharField(max_length=64)
    lecturer = models.CharField(max_length=100)
    classroom = models.CharField(max_length=64)
    semester = models.TextField(choices=SEMESTERS, default='1')
    year = models.IntegerField(choices=YEARS, default=2019)
    sks = models.PositiveIntegerField(validators=[MaxValueValidator(100)], default=1)
    difficulty = models.TextField(choices=DIFFICULTIES, default=UNKNOWN)
    desc = models.TextField()