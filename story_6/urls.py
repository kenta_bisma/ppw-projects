from django.urls import path

from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.hub, name='story_6'),
    path('add_activity/', views.hub, name='story_6_add_activity'),
    path('delete_activity/<str:pk>', views.delete_activity, name='story_6_delete_activity'),
    path('add_person/', views.hub, name='story_6_add_person'),
    path('delete_person/<str:pk>', views.delete_person, name='story_6_delete_person')
]