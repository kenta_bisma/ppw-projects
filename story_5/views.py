from django.shortcuts import render
from story_5.forms import MatkulForm
from story_5.models import MatKul
from django.shortcuts import HttpResponseRedirect


# Create your views here.
def index(request):
    return render(request, 'story_5/index.html', {'caption':'Story 5'})

def input(request):
    context = {}
    context['caption'] = "Story 5 Input"
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story_5/matkul')
    else:
        form = MatkulForm()
    context['form'] = form
    return render(request, 'story_5/input.html', context)

def matkul(request):
    context = {}
    context['caption'] = "Story 5 MatKul"

    data = MatKul.objects.all()
    context['data'] = data
    return render(request, 'story_5/matkul.html', context)

def delete_matkul(request, pk):
    matkul = MatKul.objects.get(id=pk)
    if request.method == 'POST':
        matkul.delete()
        return HttpResponseRedirect('/story_5/matkul')
    else:
        pass
    return HttpResponseRedirect('/story_5/matkul')

def edit_matkul(request, pk):
    context = {}
    context['caption'] = "Story 5 Input"

    matkul = MatKul.objects.get(id=pk)
    form = MatkulForm(instance=matkul)
    if request.method == 'POST':
        form = MatkulForm(request.POST, instance=matkul)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story_5/matkul')
    context['form'] = form
    return render(request, 'story_5/input.html', context)

def detail_matkul(request, pk):
    context = {}
    context['caption'] = "Story 5 MatKul Detail"

    datum = MatKul.objects.get(id=pk)
    context['datum'] = datum
    return render(request, 'story_5/matkul_detail.html', context)