from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_sameorigin


# Create your views here.
def index(request):
    return render(request, 'story_3/drafts.html', {'caption':'Story 3 Drafts'})

@xframe_options_sameorigin
def tooltipTest(request):
    return render(request, 'story_3/drafts/tooltip_test.html')

@xframe_options_sameorigin
def multipin(request):
    return render(request, 'story_3/drafts/multipin.html')

@xframe_options_sameorigin
def navbar(request):
    return render(request, 'story_3/drafts/navbar.html')

@xframe_options_sameorigin
def flashcards(request):
    return render(request, 'story_3/drafts/flashcards.html')

@xframe_options_sameorigin
def swappers(request):
    return render(request, 'story_3/drafts/swappers.html')

@xframe_options_sameorigin
def twoEndStick(request):
    return render(request, 'story_3/drafts/two-end-stick.html')