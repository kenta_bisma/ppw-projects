function swap(id) {
    var el = document.getElementById(id);
    var pairs = document.getElementsByClassName("swapper-pair");
    for (i = 0; i < pairs.length; i++) {
        if (pairs[i] !== el) {
            var button = pairs[i];
            button.style.order = Math.abs(pairs[i].style.order);
            var text = button.getElementsByClassName("swapper-text")[0];
            text.style.fontSize = '0';
        }
        if (el.style.order > 0) {
            el.style.order *= -1;
            var text = el.getElementsByClassName("swapper-text")[0];
            text.style.fontSize = '1.5rem';
        }
    }
}

function slidesResponse(media) {

    var slides = document.getElementsByClassName("slides")[0];
    var swappers = document.getElementsByClassName("swapper-button");
    var buttons = document.getElementsByClassName("arrow");
    if (media.matches) {
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].style.display = "none";
            swappers[i].style.width = "40px";
            swappers[i].style.height = "40px";
            swappers[i].getElementsByTagName("img")[0].style.width = "20px";
            swappers[i].getElementsByTagName("img")[0].style.height = "20px";
        }
        slides.style.width= "400px";
        slides.style.height= "400px";
    } else {
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].style.display = "inherit";
            swappers[i].style.width = "50px";
            swappers[i].style.height = "50px";
            swappers[i].getElementsByTagName("img")[0].style.width = "32px";
            swappers[i].getElementsByTagName("img")[0].style.height = "32px";
        }
        slides.style.width= "1059px";
        slides.style.height= "443px";
    }
}

var media = window.matchMedia("(max-width: 800px)");
slidesResponse(media)
media.addListener(slidesResponse)