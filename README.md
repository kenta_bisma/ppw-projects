# PPW Stories

### Description

This is my gitlab repo for Web Programming and Design class. 
[Here's the link](https://ppw-kenta.herokuapp.com/) to the site.

### Coverage - Story 9

Overall code coverage measured and reported by Python's `coverage`.

![coverage report](https://gitlab.com/kenta_bisma/ppw-projects/badges/master/coverage.svg)