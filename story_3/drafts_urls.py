from django.urls import path

from . import drafts_views

app_name = 'story_3_drafts'

urlpatterns = [
    path('', drafts_views.index, name='story_3_drafts'),
    path('tooltip_test', drafts_views.tooltipTest, name='story_3_drafts-tooltip_test'),
    path('multipin', drafts_views.multipin, name='story_3_drafts-multipin'),
    path('navbar', drafts_views.navbar, name='story_3_drafts-navbar'),
    path('flashcards', drafts_views.flashcards, name='story_3_drafts-flashcards'),
    path('swappers', drafts_views.swappers, name='story_3_drafts-swappers'),
    path('two-end-stick', drafts_views.twoEndStick, name='story_3_drafts-two-end-stick'),
]