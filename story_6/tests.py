from django.conf.urls import url
from django.http import response
from django.test import TestCase
from django.test.client import Client
from django.urls import path
from . import views
from .models import Person, Activity


# Create your tests here.
class TestStory6(TestCase):

    def test_set_up_hub(self):
        self.client = Client()
        self.url = path('', views.hub, name='story_6')
    
    def test_using_hub_template(self):
        response = Client().get("/story_6/")
        self.assertTemplateUsed(response, "story_6/hub.html")

class TestStory6Form(TestCase):

    def test_activity_form(self):
        response = Client().get("/story_6/")
        self.assertContains(response, "id_name")

    def test_activity_submit(self):
        response = Client().get("/story_6/")
        self.assertContains(response, 'type="submit"')
        self.assertContains(response, 'id="add"')
    
    def test_activity_can_save_POST_request(self):
        response = self.client.post(
            "/story_6/add_activity/",
            data={"name": "Test Activity"}
        )

        # Cek apakah masuk ke DB
        activities = Activity.objects.all().count()
        self.assertEqual(activities, 1)
        
        # Cek redirect
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["location"], "/story_6/")

        # Cek kemunculan di HTML
        response = self.client.get("/story_6/")
        self.assertContains(response, "Test Activity")

    def test_activity_can_be_deleted(self):
        pk = (str) (Activity.objects.create(name="Mabar DotA2").pk)
        response = self.client.post(
            "/story_6/delete_activity/"+pk
        )

        # Cek apakah masuk ke DB
        activities = Activity.objects.all().count()
        self.assertEqual(activities, 0)

        # Cek redirect
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["location"], "/story_6/")

        # Cek kemunculan di HTML
        response = self.client.get("/story_6/")
        self.assertNotContains(response, "Mabar DotA2")

    def test_activity_delete_button(self):
        Activity.objects.create(name="Dummy")
        response = Client().get("/story_6/")
        self.assertContains(response, 'type="submit"')
        self.assertContains(response, 'id="del"')
    
    def test_activity_add_person_button(self):
        Activity.objects.create(name="Dummy")
        response = Client().get("/story_6/")
        self.assertContains(response, 'type="submit"')
        self.assertContains(response, 'id="add-person"')
    
    def test_activity_add_person_input(self):
        Activity.objects.create(name="Dummy")
        response = Client().get("/story_6/")
        self.assertContains(response, "id_person_name")
    
    def test_activity_delete_person_button(self):
        activity = Activity.objects.create(name="Dummy")
        Person.objects.create(person_name="Dummy", activity=activity)
        response = Client().get("/story_6/")
        self.assertContains(response, 'type="submit"')
        self.assertContains(response, 'id="x"')
    
    def test_person_can_save_POST_request(self):
        activity = Activity.objects.create(name="Mabar DotA2").pk
        response = self.client.post(
            "/story_6/add_person/",
            data={
                "person_name": "Aimar",
                "activity": activity}
        )

        people = Person.objects.all().count()
        self.assertEqual(people, 1)

        # Cek redirect
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["location"], "/story_6/")

        # Cek kemunculan di HTML
        response = self.client.get("/story_6/")
        self.assertContains(response, "Aimar")

    def test_person_can_be_deleted(self):
        pk = Activity.objects.create(name="Mabar DotA2")
        person_pk = (str) (Person.objects.create(person_name="Aimar", activity=pk).pk)
        response = self.client.post(
            "/story_6/delete_person/"+person_pk
        )

        # Cek apakah masuk ke DB
        people = Person.objects.all().count()
        self.assertEqual(people, 0)

        # Cek redirect
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response["location"], "/story_6/")

        # Cek kemunculan di HTML
        response = self.client.get("/story_6/")
        self.assertNotContains(response, "Aimar")

class TestStory6Model(TestCase):

    def test_create_activity(self):
        Activity.objects.create(name = "Test")
        counting_all_available_activity = Activity.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)
    
    def test_create_person(self):
        a = Activity.objects.create(name = "Test")
        Person.objects.create(person_name = "Test", activity=a)

        counting_all_available_person = Person.objects.all().count()
        self.assertEqual(counting_all_available_person, 1)