function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("navbar").style.background = "transparent";
    } else {
        document.getElementById("navbar").style.background = "#FFFCF6";
    }
}

function navbarResponse(media) {
    var row = document.getElementsByClassName("navbar-button-row")[0];
    var els = document.getElementsByClassName("navbar-button");
    var ael = document.getElementsByClassName("navbar-href");
    if (media.matches) {
        row.style.background = "transparent";
        for (var i = 0 ; i < els.length ; i++) {
            var el = els[i];
            el.style.fontSize = "0";
            el.style.width = "40px";
            ael[i].style.margin = '5px';
            ael[i].style.background = "#FFFCF6";
            el.style.borderRadius = '20px';
        }
    } else {
        row.style.background = "#FFFCF6";
        for (var i = 0 ; i < els.length ; i++) {
            var el = els[i];
            el.style.fontSize = "16px";
            el.style.width = "140px";
            ael[i].style.margin = '0';
            ael[i].style.background = "transparent";
            el.style.borderRadius = '0';
        }
    }
}

var media = window.matchMedia("(max-width: 800px)");
navbarResponse(media)
media.addListener(navbarResponse)  