from django.http.response import JsonResponse
from django.shortcuts import redirect, render

# Create your views here.

def index(request):
    context ={'caption':"Story 8"}
    return render(request, "story_8/index.html", context)

def search(request):
    if not request.is_ajax:
        context ={'caption':"Story 8"}
        return redirect(request, "story_8/index.html", context)
    q = request.GET.get('q')
    return redirect("https://www.googleapis.com/books/v1/volumes?q="+q)
    