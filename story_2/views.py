from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'story_2/index.html', {'caption':'Story 2'})

def flow(request):
    return render(request, 'story_2/flow.html', {'caption':'Story 2 Design Flow'})

def wireframe(request):
    return render(request, 'story_2/wireframe.html', {'caption':'Story 2 Wireframe'})

def prototype(request):
    return render(request, 'story_2/prototype.html', {'caption':'Story 2 Prototype'})
