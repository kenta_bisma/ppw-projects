function search() {
    let q = $("#search_bar").val();
    $(".output").empty();
    if (q) {
        $.ajax({
            url: "./search?q=" + q,
            success: function(result) {
                if (result.items.length != 0) {
                    for (i = 0; i < result.items.length; i++) {
                        try {
                            $(".output").append(
                                "<div class='card horizontal'>" +
                                "<div class='card-image waves-effect waves-light'>" + "<a href=" +
                                result.items[i].volumeInfo.previewLink + ">" +
                                "<div class='kenta-inline-helper'></div><img src=" + result.items[i].volumeInfo.imageLinks.thumbnail + ">" + "</a>" +
                                "</div>" +
                                "<div class=card-stacked>" +
                                "<div class='card-content'><span class='truncate card-title'>" + result.items[i].volumeInfo.title +
                                "</span><span class='truncate pub'>" + (result.items[i].volumeInfo.publisher != undefined ? result.items[i].volumeInfo.publisher : "Penerbit tidak terdaftar") + "</span>" +
                                "</div>" +
                                "</div>" +
                                "</div>");
                        } catch {
                            continue
                        }
                    }
                } else {
                    $(".output").append(
                        "<div class=card style='color: #DA4167; font-weight: 300pt; font-size: 12pt;background-color: #FFCECB; padding: 16px; width: 100%;'>Tidak ada buku yang sesuai dengan query anda</div>"
                    )
                }
            }
        });
    } else {
        $(".output").append(
            "<div class=card style='color: #DA4167; font-weight: 300pt; font-size: 12pt;background-color: #FFCECB; padding: 16px; width: 100%;'>Masukkan query yang valid</div>"
        )
    }
}

$("#search").click(search)