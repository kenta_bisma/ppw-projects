from django import forms
from story_6.models import Activity, Person

class ActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = "__all__"

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = "__all__"