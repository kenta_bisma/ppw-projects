from django.urls import path

from . import views

app_name = 'story_5'

urlpatterns = [
    path('', views.index, name='story_5'),
    path('input/', views.input, name='story_5_input'),
    path('matkul/', views.matkul, name='story_5_matkul'),
    path('delete/<str:pk>', views.delete_matkul, name='story_5_delete_matkul'),
    path('edit/<str:pk>', views.edit_matkul, name='story_5_edit_matkul'),
    path('detail/<str:pk>', views.detail_matkul, name='story_5_detail_matkul'),
]