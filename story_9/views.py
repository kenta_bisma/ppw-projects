from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as log_in
from django.contrib.auth import logout as log_out
from django.contrib import messages

# Create your views here.
def index(request):
    context ={'caption':"Story 9", "name": " there", "logged": False}
    if request.user.is_authenticated:
        context["name"] = ", " + request.user.username
        context["logged"] = True
    return render(request, "story_9/index.html", context)

def login(request):
    context ={'caption':"Log In"}
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username, password=password)
        if user is not None:
            log_in(request, user)
            return redirect("/story_9/")
        else:
            messages.error(request, "Wrong username or password")

    return render(request, "story_9/login.html", context)

def logout(request):
    log_out(request)
    return redirect("story_9:story_9")

def signup(request):
    context ={'caption':"Sign Up"}
    form  = UserCreationForm()
    if request.method == 'POST':
        form  = UserCreationForm(request.POST)
        if form.is_valid:
            form.save()
            messages.success(request, "Account created!")
            return redirect("story_9:story_9_login")

    context['form'] = form
    return render(request, "story_9/signup.html", context)