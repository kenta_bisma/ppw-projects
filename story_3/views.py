from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'story_3/index.html', {'caption':'Story 3'})

def wireframe(request):
    return render(request, 'story_3/wireframe.html', {'caption':'Story 3 Wireframe'})

def main(request):
    return render(request, 'story_3/main.html', {'caption':'Story 3 Main'})