from django.urls import path

from . import views

app_name = 'story_8'

urlpatterns = [
    path('', views.index, name='story_8'),
    path('search/', views.search, name='story_8_search')
]