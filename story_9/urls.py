from django.urls import path

from . import views

app_name = 'story_9'

urlpatterns = [
    path('', views.index, name='story_9'),
    path('login/', views.login, name='story_9_login'),
    path('signup/', views.signup, name='story_9_signup'),
    path('logout/', views.logout, name='story_9_logout')
]